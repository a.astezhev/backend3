<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    print('Спасибо, результаты сохранены.');
  }
  include('form.php');
  exit();
}

//fio
$errors = FALSE;

if (empty($_POST['fio'])) {
    print('Заполните имя.<br>');
    $errors = TRUE;
}
else if (!preg_match("/^[а-яА-Яa-zA-Z ]+$/u", $_POST['fio'])) {
    print('Недопустимые символы в имени.<br>');
    $errors = TRUE;
}

//email
if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    print('Проверьте правильность ввода email<br>');
    $errors = TRUE;
}

//year
if (empty($_POST['year'])) {
    print('Заполните год.<br>');
    $errors = TRUE;
}
else {
    $year = $_POST['year'];
    if (!(is_numeric($year) && intval($year) >= 1900 && intval($year) < 2020)) {
        print("Укажите корректный год.<br>");
        $errors = TRUE;
    }
}

//abilities
$ability_data = ['immort', 'wall', 'levit', 'invis'];
if (empty($_POST['abilities'])) {
    print('Выберите способность<br>');
    $errors = TRUE;
}
else {
    $abilities = $_POST['abilities'];
    foreach ($abilities as $ability) {
        if (!in_array($ability, $ability_data)) {
            print('Недопустимая способность<br>');
            $errors = TRUE;
        }
    }
}
$ability_insert = [];
foreach ($ability_data as $ability) {
    $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
}

//accept
if (empty($_POST['accept'])) {
    print("Вы не приняли соглашение!<br>");
    $errors = TRUE;
}

if ($errors) {
  exit();
}

$user = 'u40066';
$pass = '3963958';
$abil= implode(",",$_POST['abilities']);

try {
    $db = new PDO('mysql:host=localhost;dbname=u40066', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $stmt = $db->prepare("INSERT INTO form SET fio = ?, email = ?, year = ?, sex = ?, limbs = ?, biography = ?, accept = ?");
    $stmt -> execute([$_POST['fio'], $_POST['email'], $_POST['year'], $_POST['sex'], $_POST['limbs'], $_POST['biography'], $_POST['accept']]);
    $id_user = $db->lastInsertId();

    $stmt1 = $db->prepare("INSERT INTO abilities SET id = ?, abil = ?");
    $stmt1 -> execute([$id_user, $abil]);
}

catch(PDOException $e) {
  print('Error : ' . $e->getMessage());
  exit();
}

header('Location: ?save=1');
